package atomic_broadcast

import (
	"gitlab.com/slon/shad-ds/atomic_broadcast/algo"
	"gitlab.com/slon/shad-ds/atomic_broadcast/lib"
	"gitlab.com/slon/shad-ds/atomic_broadcast/tests"
	"testing"
)

func TestExampleIsPassingExampleTests(t *testing.T) {
	lib.RunTests(t, algo.NewExample, tests.ExampleTests, false)
}

func TestExampleIsNotPassingExtraTests(t *testing.T) {
	lib.RunTests(t, algo.NewExample, tests.ExtraTests, true)
}

func TestSolutionIsPassingExampleTests(t *testing.T) {
	if algo.HasSolution {
		lib.RunTests(t, algo.NewSolution, tests.ExampleTests, false)
	}
}
