package algo

import (
	"gitlab.com/slon/shad-ds/fifo/lib"

	"container/heap"
	"fmt"
	"log"
	"os"
)

type MyMessage struct {
	value          string
	sequenceNumber int
}

func (message MyMessage) Value() string {
	return message.value
}

//////////////////////////////////////////////////////////////////////////////////

type queuedMessage struct {
	MyMessage
	source int
}

type MessageQueue []queuedMessage

func (q MessageQueue) Len() int { return len(q) }

func (q MessageQueue) Less(i, j int) bool {
	return q[i].sequenceNumber < q[j].sequenceNumber
}

func (q MessageQueue) Swap(i, j int) {
	q[i], q[j] = q[j], q[i]
}

func (q *MessageQueue) Push(x interface{}) {
	sentMessage := x.(queuedMessage)
	*q = append(*q, sentMessage)
}

func (q *MessageQueue) Pop() interface{} {
	old := *q
	n := len(old)
	item := old[n-1]
	*q = old[0 : n-1]
	return item
}

var _ heap.Interface = (*MessageQueue)(nil)

//////////////////////////////////////////////////////////////////////////////////

type exampleTransport struct {
	transport        lib.UnderlyingTransport
	node             int
	sendCounter      int
	receiveCounter   int
	queuedMessages   MessageQueue
	receivedMessages [][]lib.Message
	logger           *log.Logger
}

func (t *exampleTransport) SendMessage(value string, destination int) {
	t.logger.Printf("Send messsage (SendCounter: %v, Destination: %v, Value: %q)",
		t.sendCounter,
		destination,
		value)

	message := MyMessage{value, t.sendCounter}
	t.sendCounter++
	t.transport.SendMessage(&message, destination)
}

func (t *exampleTransport) OnMessage(message lib.Message, source int) {
	myMessage := message.(*MyMessage)

	t.logger.Printf("Queued messsage (SequenceNumber: %v, Source: %v, Value: %q)",
		myMessage.sequenceNumber,
		source,
		myMessage.value)

	heap.Push(
		&t.queuedMessages,
		queuedMessage{
			MyMessage: *myMessage,
			source:    source})

	for len(t.queuedMessages) > 0 && t.queuedMessages[0].sequenceNumber == t.receiveCounter {
		t.receiveCounter++
		currentMessage := heap.Pop(&t.queuedMessages).(queuedMessage)
		for len(t.receivedMessages) <= currentMessage.source {
			t.receivedMessages = append(t.receivedMessages, make([]lib.Message, 0))
		}
		messages := &t.receivedMessages[currentMessage.source]
		*messages = append(*messages, lib.Message(currentMessage))

		t.logger.Printf("Received messsage (SequenceNumber: %v, Source: %v, Value: %q)",
			currentMessage.sequenceNumber,
			currentMessage.source,
			currentMessage.value)
	}
}

func (t *exampleTransport) ReceivedValues(source int) []string {
	receivedMessages := t.receivedMessages[source]
	result := make([]string, 0, len(receivedMessages))

	for _, message := range receivedMessages {
		result = append(result, message.Value())
	}

	return result
}

//////////////////////////////////////////////////////////////////////////////////

func NewExample(node int, transport lib.UnderlyingTransport) lib.FifoTransport {
	logger := log.New(os.Stderr, fmt.Sprintf("Node: %v | ", node), log.Lmicroseconds)
	fifoTransport := &exampleTransport{
		transport:        transport,
		node:             node,
		queuedMessages:   make(MessageQueue, 0),
		receivedMessages: make([][]lib.Message, 0),
		logger:           logger}

	return lib.FifoTransport(fifoTransport)
}
