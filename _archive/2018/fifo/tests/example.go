package tests

import (
	"gitlab.com/slon/shad-ds/fifo/lib"
	"reflect"
)

func equal(a, b []string) bool {
	return reflect.DeepEqual(a, b)
}

var ExampleTests = []lib.TestDescriptor{
	{

		Author:    "psushin@yandex-team.ru",
		TestName:  "2 nodes - 1/0 message",
		NodeCount: 2,
		SendRequests: []lib.SendRequest{
			{
				Timestamp:   0,
				Source:      0,
				Destination: 1,
				Value:       "msg",
			},
		},
		DelayFunc: func(message lib.SentMessage) int {
			return message.SendTimestamp + 5
		},
		Validators: []lib.Validator{
			{
				Timestamp: lib.MaxTimestamp - 1,
				Node:      1,
				Source:    0,
				Validate: func(values []string) bool {
					return equal(values, []string{"msg"})
				},
			},
		},
	},
	{
		Author:    "psushin@yandex-team.ru",
		TestName:  "2 nodes - 3/0 messages",
		NodeCount: 2,
		SendRequests: []lib.SendRequest{
			{
				Timestamp:   0,
				Source:      0,
				Destination: 1,
				Value:       "msg1",
			},
			{
				Timestamp:   5,
				Source:      0,
				Destination: 1,
				Value:       "msg2",
			},
			{
				Timestamp:   10,
				Source:      0,
				Destination: 1,
				Value:       "msg3",
			},
		},
		DelayFunc: func(message lib.SentMessage) int {
			switch message.Message.Value() {
			case "msg1":
				return 100
			case "msg2":
				return 300
			case "msg3":
				return 200
			default:
				panic("Unexpected message")
			}
		},
		Validators: []lib.Validator{
			{
				Timestamp: 150,
				Node:      1,
				Source:    0,
				Validate: func(values []string) bool {
					return equal(values, []string{"msg1"})
				},
			},
			{
				Timestamp: lib.MaxTimestamp - 1,
				Node:      1,
				Source:    0,
				Validate: func(values []string) bool {
					return equal(values, []string{"msg1", "msg2", "msg3"})
				},
			},
		},
	},
}

var BadTest = []lib.TestDescriptor{
	{
		Author:    "psushin@yandex-team.ru",
		TestName:  "invalid message handler",
		NodeCount: 2,
		SendRequests: []lib.SendRequest{
			{
				Timestamp:   0,
				Source:      0,
				Destination: 1,
				Value:       "msg",
			},
		},
		DelayFunc: func(message lib.SentMessage) int {
			panic("Good luck, I quit!")
		},
		Validators: []lib.Validator{},
	},
}
