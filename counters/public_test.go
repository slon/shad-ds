package counters

import (
	"testing"

	"gitlab.com/slon/shad-ds/counters/lib"
	"gitlab.com/slon/shad-ds/counters/solution"
	"gitlab.com/slon/shad-ds/counters/tests"
)

func TestPublic(t *testing.T) {
	lib.RunTests(t, solution.NewCountersService, tests.PublicTests)
}
