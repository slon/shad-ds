package solution

import (
	"gitlab.com/slon/shad-ds/counters/interfaces"

	"fmt"
)

/////////////////////////////////////////////////////////////////////////////////////////////

type CountersActor struct {
	context   interfaces.ActorContext
	nodeIndex int
	nodeCount int
}

func (a *CountersActor) Receive(message interfaces.Message) {
	switch m := message.Payload.(type) {
	case *interfaces.IncrementCounterReq:
		err := a.context.Send(&interfaces.IncrementCounterRsp{RequestId: m.RequestId}, message.Sender)
		if err != nil {
			panic(fmt.Errorf("error sending response: %v", err))
		}
	case *interfaces.GetCounterReq:
		err := a.context.Send(&interfaces.GetCounterRsp{RequestId: m.RequestId, Value: 0}, message.Sender)
		if err != nil {
			panic(fmt.Errorf("error sending response: %v", err))
		}
	default:
		panic(fmt.Errorf("unknown message %v", m))
	}
}

func NewCountersService(context interfaces.ActorContext, nodeIndex int, nodeCount int) interfaces.Actor {
	return &CountersActor{
		context:   context,
		nodeIndex: nodeIndex,
		nodeCount: nodeCount,
	}
}
