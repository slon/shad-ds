// +build private

package projects

import (
	"testing"

	"gitlab.com/slon/shad-ds/private/projects/tests"
	"gitlab.com/slon/shad-ds/projects/lib"
	"gitlab.com/slon/shad-ds/projects/solution"
)

func TestPrivate(t *testing.T) {
	lib.RunTests(t, solution.NewUsersService, solution.NewProjectsService, tests.PrivateTests)
}
