package tests

import (
	"fmt"
	"time"

	"go.uber.org/atomic"

	"gitlab.com/slon/shad-ds/projects/lib"
)

func sliceContains(value string, slice []string) bool {
	for _, element := range slice {
		if element == value {
			return true
		}
	}
	return false
}

func slicesEqual(lhs, rhs []string) bool {
	if len(lhs) != len(rhs) {
		return false
	}
	for i := range lhs {
		if lhs[i] != rhs[i] {
			return false
		}
	}
	return true
}

/////////////////////////////////////////////////////////////////////////////////////////////

func simpleClient(actorSystem *lib.ActorSystem) error {
	client := lib.NewClient(actorSystem)

	if err := <-client.CreateUser("psushin", []string{"YMR"}); err == nil {
		return fmt.Errorf("successfully created user \"psushin\" in a non-existent project \"YMR\"")
	}

	if err := <-client.CreateProject("YT", []string{"psushin"}); err == nil {
		return fmt.Errorf("successfully created project \"YT\" with a non-existent user \"psushin\"")
	}

	if err := <-client.CreateUser("psushin", []string{}); err != nil {
		return err
	}

	if err := <-client.CreateUser("psushin", []string{}); err == nil {
		return fmt.Errorf("successfully created user \"psushin\" twice")
	}

	if err := <-client.CreateProject("YT", []string{"psushin"}); err != nil {
		return err
	}

	if err := <-client.CreateProject("YT", []string{"psushin"}); err == nil {
		return fmt.Errorf("successfully created project \"YT\" twice")
	}

	for _, c := range []bool{false, true} {
		r := <-client.GetUserProjects("psushin", c)
		if r.Err != nil {
			return r.Err
		}
		if !slicesEqual([]string{"YT"}, r.Values) {
			return fmt.Errorf("expected user \"psushin\" to be a member of the only project \"YT\", but got %q",
				r.Values)
		}
	}

	for _, c := range []bool{false, true} {
		r := <-client.GetProjectUsers("YT", c)
		if r.Err != nil {
			return r.Err
		}
		if !slicesEqual([]string{"psushin"}, r.Values) {
			return fmt.Errorf("expected project \"YT\" to contain only user \"psushin\", but got %q",
				r.Values)
		}
		return nil
	}

	return nil
}

func createSimpleTest() lib.TestDescriptor {
	fixedDelay := func(message lib.SentMessage) int {
		return message.SendTimestamp + 5
	}

	return lib.TestDescriptor{
		TestName:     "Simple test",
		Clients:      []lib.ClientFunc{simpleClient},
		DelayMessage: fixedDelay,
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////

func twoPhaseClient(actorSystem *lib.ActorSystem, stopDeliveryFlag *atomic.Bool) error {
	validateGetResult := func(rsp lib.GetResponse, expected []string) error {
		if rsp.Err != nil {
			return rsp.Err
		}

		for _, value := range expected {
			if !sliceContains(value, rsp.Values) {
				return fmt.Errorf("response does not contain expected value %q in %q", value, rsp.Values)
			}
		}

		return nil
	}

	client := lib.NewClient(actorSystem)

	if err := <-client.CreateUser("psushin", []string{}); err != nil {
		return err
	}

	if err := <-client.CreateUser("bidzilya", []string{}); err != nil {
		return err
	}

	if err := <-client.CreateProject("YT", []string{"psushin", "bidzilya"}); err != nil {
		return err
	}

	if err := <-client.CreateProject("Xurma", []string{"psushin"}); err != nil {
		return err
	}

	if err := <-client.CreateUser("galtsev", []string{"Xurma"}); err != nil {
		return err
	}

	stopDeliveryFlag.Store(true)

	// Start inconsistent reads.
	getResult1 := <-client.GetUserProjects("psushin", false)
	if err := validateGetResult(getResult1, []string{"YT", "Xurma"}); err != nil {
		return err
	}

	getResult2 := <-client.GetUserProjects("bidzilya", false)
	if err := validateGetResult(getResult2, []string{"YT"}); err != nil {
		return err
	}

	getResult3 := <-client.GetProjectUsers("YT", false)
	if err := validateGetResult(getResult3, []string{"psushin", "bidzilya"}); err != nil {
		return err
	}

	getResult4 := <-client.GetProjectUsers("Xurma", false)
	if err := validateGetResult(getResult4, []string{"galtsev", "psushin"}); err != nil {
		return err
	}

	return nil
}

func createTwoPhaseTest() lib.TestDescriptor {
	stopDeliveryFlag := atomic.NewBool(false)

	twoPhaseDelay := func(message lib.SentMessage) int {
		if stopDeliveryFlag.Load() {
			return lib.MaxTimestamp + 1
		} else {
			return message.SendTimestamp + 5
		}
	}

	createClient := func(system *lib.ActorSystem) error {
		return twoPhaseClient(system, stopDeliveryFlag)
	}

	return lib.TestDescriptor{
		TestName:     "Two phase test",
		Clients:      []lib.ClientFunc{createClient},
		DelayMessage: twoPhaseDelay,
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////

type ConcurrentCreatesTestClientState struct {
	user          string
	otherUser     string
	preparedCount []*atomic.Int32
	errorCount    []*atomic.Int32
}

func concurrentCreatesClient(actorSystem *lib.ActorSystem, state ConcurrentCreatesTestClientState) error {
	client := lib.NewClient(actorSystem)

	if err := <-client.CreateUser(state.user, []string{}); err != nil {
		return err
	}

	for exp := range state.preparedCount {
		state.preparedCount[exp].Inc()
		for state.preparedCount[exp].Load() < 2 {
			time.Sleep(time.Millisecond)
		}
		project := fmt.Sprintf("FancyProject#%v", exp)
		if err := <-client.CreateProject(project, []string{state.user}); err != nil {
			if state.errorCount[exp].Inc() > 1 {
				return err
			}
		} else {
			result := <-client.GetProjectUsers(project, true)
			if result.Err != nil {
				return result.Err
			}
			users := result.Values
			if !slicesEqual(users, []string{state.user}) &&
				!slicesEqual(users, []string{state.user, state.otherUser}) &&
				!slicesEqual(users, []string{state.otherUser, state.user}) {
				return fmt.Errorf("expected to have mandatory user %q and optional user %q in project %q, but got %q users",
					state.user,
					state.otherUser,
					project,
					users)
			}
		}
	}

	return nil
}

func createConcurrentCreatesTest() lib.TestDescriptor {
	fixedDelay := func(message lib.SentMessage) int {
		return message.SendTimestamp + 5
	}

	expCount := 50

	preparedCount := make([]*atomic.Int32, expCount)
	errorCount := make([]*atomic.Int32, expCount)
	for i := 0; i < expCount; i++ {
		preparedCount[i] = atomic.NewInt32(0)
		errorCount[i] = atomic.NewInt32(0)
	}

	createClient := func(system *lib.ActorSystem, user, otherUser string) error {
		return concurrentCreatesClient(
			system,
			ConcurrentCreatesTestClientState{
				user:          user,
				otherUser:     otherUser,
				preparedCount: preparedCount,
				errorCount:    errorCount,
			},
		)
	}

	user1 := "abra"
	user2 := "cadabra"

	firstClient := func(system *lib.ActorSystem) error {
		return createClient(system, user1, user2)
	}
	secondClient := func(system *lib.ActorSystem) error {
		return createClient(system, user2, user1)
	}

	return lib.TestDescriptor{
		TestName:     "Concurrent creates test",
		Clients:      []lib.ClientFunc{firstClient, secondClient},
		DelayMessage: fixedDelay,
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////

var PublicTests = []lib.TestDescriptor{
	createSimpleTest(),
	createTwoPhaseTest(),
	createConcurrentCreatesTest(),
}
