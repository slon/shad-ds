package lib

import (
	"fmt"
	"gitlab.com/slon/shad-ds/raft/interfaces"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
	"io"
	"math/rand"
)

type PersistentActorContext struct {
	NodeId int
	Alive  bool
	System *ActorSystem

	PersistentStorage []byte

	// Messages delivered on all incarnations of this node.
	DeliveredBroadcastValues [][]int64
	Incarnation              int

	CurrentIncarnationContext *IncarnationContext
	CurrentNode               interfaces.Actor

	StableBroadcastValues           map[int64]bool
	UnconfirmedStableBroadcastCount int

	Log *zap.SugaredLogger
}

func NewPersistentActorContext(nodeId int, system *ActorSystem) *PersistentActorContext {
	context := &PersistentActorContext{
		PersistentStorage: make([]byte, 0),
		NodeId:            nodeId,
		Alive:             true,
		System:            system,
		DeliveredBroadcastValues: make([][]int64, 1),
		Incarnation:              -1,
		Log:                      system.Log.With("actor", interfaces.GetActorId(nodeId))}

	context.Revive()
	return context
}

func (c *PersistentActorContext) ProcessTestEvent(testEvent *TestEvent) {
	switch testEvent.Type {
	case Broadcast:
		if c.Alive {
			c.CurrentNode.AtomicBroadcast(testEvent.BroadcastValue)
		}

	case Death:
		if c.Alive {
			c.Alive = false
			c.CurrentIncarnationContext.Alive = false
			c.CurrentIncarnationContext = nil
			c.CurrentNode = nil
		}

	case Revival:
		if !c.Alive {
			c.Alive = true
			c.Revive()
		}

	default:
		panic("Unknown test event")
	}
}

func (c *PersistentActorContext) Revive() {
	c.Alive = true
	c.Incarnation++
	c.DeliveredBroadcastValues = append(c.DeliveredBroadcastValues, make([]int64, 0))
	incarnationContext := &IncarnationContext{
		Alive:             true,
		PersistentContext: c}

	c.CurrentIncarnationContext = incarnationContext
	c.CurrentNode = c.System.Ctor(
		c.NodeId,
		c.System.NodeCount,
		incarnationContext,
		incarnationContext)
}

func (c *PersistentActorContext) OnMessage(msg *SentMessage) {
	if c.Alive {
		if msg.TimerCallback != nil {
			(*msg.TimerCallback)()
		} else {
			payload := proto.Clone(msg.PayloadThunk)
			if err := proto.Unmarshal(msg.Payload, payload); err != nil {
				panic(err)
			}

			message := interfaces.Message{
				Payload: payload,
				Sender:  msg.Sender,
			}

			c.CurrentNode.Receive(message)
		}
	}
}

func (c *PersistentActorContext) RegisterStableBroadcasts(broadcasts []BroadcastRequest) {
	c.UnconfirmedStableBroadcastCount = len(broadcasts)
	c.StableBroadcastValues = make(map[int64]bool)
	for _, broadcast := range broadcasts {
		c.StableBroadcastValues[broadcast.BroadcastValue] = false
	}
}

func (c *PersistentActorContext) DeliveredAllStableBroadcasts() bool {
	return c.UnconfirmedStableBroadcastCount == 0
}

func (c *PersistentActorContext) Send(payload proto.Message, receiver string) error {
	if !c.System.ActorExists(receiver) {
		return fmt.Errorf("actor %q does not exist", receiver)
	}

	serializedMessage, err := proto.Marshal(payload)
	if err != nil {
		return err
	}

	if len(serializedMessage) > interfaces.MaxMessageSize {
		return fmt.Errorf("too large message size %v bytes", len(serializedMessage))
	}

	thunk := proto.Clone(payload)
	proto.Reset(thunk)

	sentMessage := SentMessage{
		Sender:           interfaces.GetActorId(c.NodeId),
		Receiver:         receiver,
		Payload:          serializedMessage,
		PayloadThunk:     thunk,
		SendTimestamp:    c.System.Timestamp,
		DeliverTimestamp: c.System.Timestamp + interfaces.AvgDeliveryTime,
		TimerCallback:    nil}

	c.System.ApplyDelayFunc(&sentMessage)
	c.System.Queue.EnqueueMessage(sentMessage)
	return nil
}

func (c *PersistentActorContext) AfterFunc(duration int, f interfaces.TimerCallback) {
	sentMessage := SentMessage{
		TimerCallback:    &f,
		Sender:           interfaces.GetActorId(c.NodeId),
		Receiver:         interfaces.GetActorId(c.NodeId),
		SendTimestamp:    c.System.Timestamp,
		DeliverTimestamp: c.System.Timestamp + duration}

	c.System.ApplyDelayFunc(&sentMessage)
	c.System.Queue.EnqueueMessage(sentMessage)
}

func (c *PersistentActorContext) WriteAt(p []byte, off int64) (n int, err error) {
	offset := int(off)
	if len(c.PersistentStorage) < offset+len(p) {
		size := offset + len(p) - len(c.PersistentStorage)
		c.PersistentStorage = append(c.PersistentStorage, make([]byte, size)...)
	}

	n = copy(c.PersistentStorage[offset:offset+len(p)], p)
	if n != len(p) {
		err = fmt.Errorf("Fatal error during writing to persistent storage")
	}
	return
}

func (c *PersistentActorContext) ReadAt(p []byte, off int64) (n int, err error) {
	offset := int(off)
	if len(c.PersistentStorage) < offset {
		n = 0
		err = fmt.Errorf("Offset is too large")
		return
	}

	n = len(p)
	if len(c.PersistentStorage)-offset < n {
		n = len(c.PersistentStorage) - offset
		err = io.EOF
	}

	copy(p, c.PersistentStorage[offset:offset+n])
	return
}

func (c *PersistentActorContext) Deliver(broadcastValue int64) {
	c.DeliveredBroadcastValues[c.Incarnation] = append(c.DeliveredBroadcastValues[c.Incarnation], broadcastValue)
	value, ok := c.StableBroadcastValues[broadcastValue]
	if ok && !value {
		c.UnconfirmedStableBroadcastCount--
		c.StableBroadcastValues[broadcastValue] = true
	}
}

func (c *PersistentActorContext) OnStartLeading(term int64) {
	c.System.RegisterStartLeading(term, c.NodeId)
}

//////////////////////////////////////////////////////////////////////////////////

type IncarnationContext struct {
	Alive             bool
	PersistentContext *PersistentActorContext
}

func (ic *IncarnationContext) Send(message proto.Message, destination string) error {
	if !ic.Alive {
		return fmt.Errorf("Cannot send message, since actor is dead :(")
	}

	return ic.PersistentContext.Send(message, destination)
}

func (ic *IncarnationContext) AfterFunc(duration int, f interfaces.TimerCallback) {
	if !ic.Alive {
		return
	}

	ic.PersistentContext.AfterFunc(duration, f)
}

func (ic *IncarnationContext) RandomInt(n int) int {
	return rand.Intn(n)
}

func (ic *IncarnationContext) WriteAt(p []byte, off int64) (n int, err error) {
	if !ic.Alive {
		n = 0
		err = fmt.Errorf("Peer is dead")
		return
	}

	return ic.PersistentContext.WriteAt(p, off)
}

func (ic *IncarnationContext) ReadAt(p []byte, off int64) (n int, err error) {
	if !ic.Alive {
		n = 0
		err = fmt.Errorf("Peer is dead")
		return
	}

	return ic.PersistentContext.ReadAt(p, off)
}

func (ic *IncarnationContext) Deliver(broadcastValue int64) {
	if !ic.Alive {
		return
	}

	ic.PersistentContext.Deliver(broadcastValue)
}

func (ic *IncarnationContext) OnStartLeading(term int64) {
	ic.PersistentContext.OnStartLeading(term)
}

func (ic *IncarnationContext) GetStorageLength() int {
	if !ic.Alive {
		return 0
	}

	return len(ic.PersistentContext.PersistentStorage)
}

func (ic *IncarnationContext) Log() *zap.SugaredLogger {
	return ic.PersistentContext.Log
}
