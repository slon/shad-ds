package lib

import (
	"container/heap"
	"fmt"
	"gitlab.com/slon/shad-ds/raft/interfaces"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
	"math"
	"math/rand"
	"runtime/debug"
	"testing"
	"time"
)

//////////////////////////////////////////////////////////////////////////////////

type EventType int

const (
	Death        EventType = 0
	Revival      EventType = 1
	Broadcast    EventType = 2
	FinishWarmup EventType = 3
)

type TestEvent struct {
	ActorId        string
	Timestamp      int
	Type           EventType
	BroadcastValue int64
}

type CombinedQueue struct {
	queueImpl  []interface{}
	eventCount int
}

func NewCombinedQueue() *CombinedQueue {
	queue := &CombinedQueue{
		queueImpl:  make([]interface{}, 0, 10),
		eventCount: 0}

	heap.Init(queue)
	return queue
}

func (q CombinedQueue) Len() int { return len(q.queueImpl) }

func (q CombinedQueue) Less(i, j int) bool {
	getTimestamp := func(item interface{}) int {
		switch x := item.(type) {
		case SentMessage:
			return x.DeliverTimestamp

		case TestEvent:
			return x.Timestamp
		}

		panic(fmt.Errorf("unreachable"))
	}

	return getTimestamp(q.queueImpl[i]) < getTimestamp(q.queueImpl[j])
}

func (q CombinedQueue) Swap(i, j int) {
	q.queueImpl[i], q.queueImpl[j] = q.queueImpl[j], q.queueImpl[i]
}

func (q *CombinedQueue) Push(x interface{}) {
	q.queueImpl = append(q.queueImpl, x)
}

func (q *CombinedQueue) Pop() interface{} {
	old := *q
	n := len(old.queueImpl)
	item := old.queueImpl[n-1]
	q.queueImpl = old.queueImpl[0 : n-1]
	return item
}

func (q *CombinedQueue) EnqueueBroadcasts(broadcasts []BroadcastRequest, currentTimestamp int) {
	for _, broadcast := range broadcasts {
		event := TestEvent{
			ActorId:        broadcast.Source,
			Timestamp:      broadcast.Timestamp + currentTimestamp,
			Type:           Broadcast,
			BroadcastValue: broadcast.BroadcastValue}
		heap.Push(q, event)
		q.eventCount++
	}
}

func (q *CombinedQueue) EnqueueNodeDeaths(nodeEvents []NodeEvent, currentTimestamp int) {
	for _, death := range nodeEvents {
		event := TestEvent{
			ActorId:   death.Node,
			Timestamp: death.Timestamp + currentTimestamp,
			Type:      Death}
		heap.Push(q, event)
		q.eventCount++
	}
}

func (q *CombinedQueue) EnqueueFinishWarmup(timestamp int) {
	event := TestEvent{
		ActorId:   "<NONE>",
		Timestamp: timestamp,
		Type:      FinishWarmup}
	heap.Push(q, event)
	q.eventCount++
}

func (q *CombinedQueue) EnqueueNodeRevivals(nodeEvents []NodeEvent, currentTimestamp int) {
	for _, revival := range nodeEvents {
		event := TestEvent{
			ActorId:   revival.Node,
			Timestamp: revival.Timestamp + currentTimestamp,
			Type:      Revival}
		heap.Push(q, event)
		q.eventCount++
	}
}

func (q *CombinedQueue) EnqueueMessage(sentMessage SentMessage) {
	heap.Push(q, sentMessage)
}

func (q *CombinedQueue) GetNext() interface{} {
	if len(q.queueImpl) == 0 {
		return nil
	}

	item := heap.Pop(q)
	if _, ok := item.(TestEvent); ok {
		q.eventCount--
	}
	return item
}

func (q CombinedQueue) HasEnqueuedEvents() bool {
	return q.eventCount > 0
}

//////////////////////////////////////////////////////////////////////////////////

var DelayRand = rand.New(rand.NewSource(7919))

func DefaultDelayFunc(sentMessage SentMessage) int {
	delay := sentMessage.DeliverTimestamp - sentMessage.SendTimestamp
	r := DelayRand.NormFloat64()
	r = 2*r - 1.0 // get random in range [-1, 1]
	r = r * 0.05  // get random in range [-0.05, 0.05]
	delta := int(float64(delay) * r)
	return sentMessage.DeliverTimestamp + delta
}

//////////////////////////////////////////////////////////////////////////////////

type PhaseType int

const (
	Warmup   PhaseType = 0
	Stable   PhaseType = 1
	Unstable PhaseType = 2
	Teardown PhaseType = 3
)

type StartLeadingEvent struct {
	nodeId int
	term   int64
}

type ActorSystem struct {
	Timestamp            int
	CurrentSegment       *TestSegment
	CurrentSegmentIndex  int
	Phase                PhaseType
	Ctor                 interfaces.AtomicBroadcasterCtor
	NodeCount            int
	Actors               []string
	Queue                *CombinedQueue
	PersistentContexts   map[string]*PersistentActorContext
	StartedLeadingEvents []StartLeadingEvent
	Log                  *zap.SugaredLogger
}

func (state ActorSystem) ApplyDelayFunc(sentMessage *SentMessage) {

	validateNoJumpsIntoPast := func(ts int) {
		if ts <= sentMessage.SendTimestamp {
			panic(fmt.Errorf("invalid delay func in unstable sub-segment %v; deliver timestamp (%v) must be greater than send timestamp (%v)",
				state.CurrentSegmentIndex,
				ts,
				sentMessage.DeliverTimestamp))
		}
	}

	validateBoundedDrift := func(ts int) {
		originalDelay := sentMessage.DeliverTimestamp - sentMessage.SendTimestamp
		actualDelay := ts - sentMessage.SendTimestamp

		delta := originalDelay - actualDelay
		drift := float64(delta) / float64(originalDelay)
		if math.Abs(drift) > StableSegmentError {
			panic(fmt.Errorf("invalid delay func in stable sub-segment %v; deliver timestamp drift (%v) is too large ",
				state.CurrentSegmentIndex,
				drift))
		}
	}

	switch {
	case state.CurrentSegment == nil:
		timestamp := DefaultDelayFunc(*sentMessage)
		sentMessage.DeliverTimestamp = timestamp
	case state.Phase == Unstable:
		timestamp := state.CurrentSegment.UnstableDelayFunc(*sentMessage)
		validateNoJumpsIntoPast(timestamp)
		sentMessage.DeliverTimestamp = timestamp
	default:
		timestamp := state.CurrentSegment.StableDelayFunc(*sentMessage)
		validateNoJumpsIntoPast(timestamp)
		validateBoundedDrift(timestamp)
		sentMessage.DeliverTimestamp = timestamp
	}
}

func (state ActorSystem) ValidateActiveQuorum() error {
	aliveNodeCount := 0
	for _, context := range state.PersistentContexts {
		if context.Alive {
			aliveNodeCount++
		}
	}

	if aliveNodeCount <= len(state.Actors)/2 {
		return fmt.Errorf("invalid test: not enough alive nodes in the beginning of segment %v", state.CurrentSegmentIndex)
	}

	return nil
}

func (state ActorSystem) ValidateStableBroadcasts(broadcasts []BroadcastRequest, segmentIndex int) error {
	for _, broadcast := range broadcasts {
		context := state.PersistentContexts[broadcast.Source]
		if !context.Alive {
			return fmt.Errorf("invalid test: cannot broadcast message with value %v on dead node %v in stable segment %v",
				broadcast.BroadcastValue,
				broadcast.Source,
				segmentIndex)
		}
	}

	return nil
}

func (state ActorSystem) ActorExists(actor string) bool {
	for _, actorId := range state.Actors {
		if actor == actorId {
			return true
		}
	}
	return false
}

func (state *ActorSystem) ReviveAll() {
	for _, context := range state.PersistentContexts {
		if !context.Alive {
			context.Revive()
		}
	}
}

func (state *ActorSystem) RunEventLoopWhile(condition func() bool) {
	for condition() {
		event := state.Queue.GetNext()
		switch x := event.(type) {
		case SentMessage:
			state.Timestamp = x.DeliverTimestamp
			context := state.PersistentContexts[x.Receiver]
			if x.TimerCallback != nil {
				// Call delayed callback.
				(*x.TimerCallback)()
			} else {
				context.OnMessage(&x)
			}

		case TestEvent:
			state.Timestamp = x.Timestamp
			if x.Type != FinishWarmup {
				context := state.PersistentContexts[x.ActorId]
				context.ProcessTestEvent(&x)
			}
		}
	}
}

func (state *ActorSystem) ValidateAllDeliveries() error {
	reference := state.Actors[0]
	context := state.PersistentContexts[reference]
	for _, actor := range state.Actors[1:] {
		other := state.PersistentContexts[actor]
		if len(context.DeliveredBroadcastValues[context.Incarnation]) != len(other.DeliveredBroadcastValues[other.Incarnation]) {
			return fmt.Errorf("different number of delivered messages in nodes %v and %v",
				context.NodeId,
				other.NodeId)
		}

		for logIndex := 0; logIndex < len(context.DeliveredBroadcastValues[context.Incarnation]); logIndex++ {
			if context.DeliveredBroadcastValues[context.Incarnation][logIndex] != other.DeliveredBroadcastValues[other.Incarnation][logIndex] {
				return fmt.Errorf("nodes %v and %v have different message delivered at log position %v",
					context.NodeId,
					other.NodeId,
					logIndex)
			}
		}
	}

	for _, context := range state.PersistentContexts {
		for incarnation := 0; incarnation < context.Incarnation; incarnation++ {
			if len(context.DeliveredBroadcastValues[incarnation]) > len(context.DeliveredBroadcastValues[context.Incarnation]) {
				return fmt.Errorf("last incarnation of actor %v has less delivered events that %v incarnation (%v < %v)",
					context.NodeId,
					incarnation,
					len(context.DeliveredBroadcastValues[context.Incarnation]),
					len(context.DeliveredBroadcastValues[incarnation]))
			}

			for logIndex := 0; logIndex < len(context.DeliveredBroadcastValues[incarnation]); logIndex++ {
				if context.DeliveredBroadcastValues[context.Incarnation][logIndex] != context.DeliveredBroadcastValues[incarnation][logIndex] {
					return fmt.Errorf("actorId %v has different message delivered at log position %v in incarnations %v and %v (%v != %v)",
						context.NodeId,
						logIndex,
						context.Incarnation,
						incarnation,
						context.DeliveredBroadcastValues[context.Incarnation][logIndex],
						context.DeliveredBroadcastValues[incarnation][logIndex])
				}
			}
		}
	}

	return nil
}

func (state *ActorSystem) RegisterStartLeading(term int64, nodeId int) {
	state.StartedLeadingEvents = append(state.StartedLeadingEvents, StartLeadingEvent{nodeId, term})
}

//////////////////////////////////////////////////////////////////////////////////

func (state ActorSystem) ValidateStartLeading() error {
	registeredTerms := map[int64]int{}
	for _, event := range state.StartedLeadingEvents {
		_, ok := registeredTerms[event.term]
		if ok {
			return fmt.Errorf("duplicate start leading events with the same term %v", event.term)
		}
		registeredTerms[event.term] = event.nodeId
	}

	return nil
}

func ValidateUniqueMessageIds(segments []TestSegment) error {
	uniqueBroadcasts := map[int64]bool{}

	totalBroadcastCount := 0
	for _, segment := range segments {
		for _, broadcast := range segment.StableBroadcasts {
			_, ok := uniqueBroadcasts[broadcast.BroadcastValue]
			totalBroadcastCount++
			if ok {
				return fmt.Errorf("invalid test: Duplicate broadcast values in broadcast requests (BroadcastValue: %v)", broadcast.BroadcastValue)
			}
			uniqueBroadcasts[broadcast.BroadcastValue] = true
		}

		for _, broadcast := range segment.UnstableBroadcasts {
			_, ok := uniqueBroadcasts[broadcast.BroadcastValue]
			totalBroadcastCount++
			if ok {
				return fmt.Errorf("invalid test: Duplicate broadcast values in broadcast requests (BroadcastValue: %v)", broadcast.BroadcastValue)
			}
			uniqueBroadcasts[broadcast.BroadcastValue] = true
		}
	}

	if totalBroadcastCount > MaxBroadcastsPerTest {
		return fmt.Errorf("invalid test: total number of broadcasts is too large (BroadcastCount: %v)", totalBroadcastCount)
	}

	return nil
}

func ValidateEventCount(segments []TestSegment) error {
	totalEventCount := 0
	for _, segment := range segments {
		totalEventCount += len(segment.DeathEvents)
		totalEventCount += len(segment.ReviveEvents)
	}

	if totalEventCount > MaxEventsPerTest {
		return fmt.Errorf("invalid test: total number of deaths and revivals is too large (EventCount: %v)", totalEventCount)
	}

	return nil
}

func RunTest(test TestDescriptor, nodeCtor interfaces.AtomicBroadcasterCtor, log *zap.SugaredLogger) (err error) {
	const RandomSeed = 7919
	DelayRand.Seed(RandomSeed)
	rand.Seed(RandomSeed)

	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("%v\nStack:\n%s\n", e, debug.Stack())
		}
	}()

	if test.NodeCount < 1 {
		return fmt.Errorf("invalid test: NodeCount must be greater than zero (NodeCount: %v)",
			test.NodeCount)
	}

	if test.NodeCount > MaxNodeCount {
		return fmt.Errorf("invalid test: NodeCount must not be greater than MaxNodeCount (NodeCount: %v)",
			test.NodeCount)
	}

	if len(test.Segments) > MaxSegmentsPerTest {
		return fmt.Errorf("invalid test: number of segments in test should not exceed than MaxSegmentsPerTest")
	}

	if err = ValidateUniqueMessageIds(test.Segments); err != nil {
		return
	}

	if err = ValidateEventCount(test.Segments); err != nil {
		return
	}

	if len(test.Segments) < 1 {
		return
	}

	actorSystem := &ActorSystem{
		Timestamp:            0,
		Ctor:                 nodeCtor,
		Phase:                Warmup,
		NodeCount:            test.NodeCount,
		Actors:               []string{},
		Queue:                NewCombinedQueue(),
		PersistentContexts:   map[string]*PersistentActorContext{},
		StartedLeadingEvents: []StartLeadingEvent{},
		Log:                  log}

	for i := 0; i < test.NodeCount; i++ {
		actorId := interfaces.GetActorId(i)
		actorSystem.Actors = append(actorSystem.Actors, actorId)
		actorSystem.PersistentContexts[actorId] = NewPersistentActorContext(i, actorSystem)
	}

	for segmentIndex, segment := range test.Segments {
		log.Infow("Start segment", "segmentIndex", segmentIndex, "timestamp", actorSystem.Timestamp)

		if err = actorSystem.ValidateActiveQuorum(); err != nil {
			return
		}

		actorSystem.CurrentSegment = &segment
		actorSystem.CurrentSegmentIndex = segmentIndex
		actorSystem.Phase = Warmup

		warmupTimestamp := actorSystem.Timestamp + interfaces.AvgDeliveryTime*WarmupRounds
		actorSystem.Queue.EnqueueFinishWarmup(warmupTimestamp + 1)
		actorSystem.RunEventLoopWhile(func() bool {
			return actorSystem.Timestamp < warmupTimestamp
		})

		log.Infow("Finished warmup", "segmentIndex", segmentIndex, "timestamp", actorSystem.Timestamp)

		actorSystem.Phase = Stable
		if err = actorSystem.ValidateStableBroadcasts(segment.StableBroadcasts, segmentIndex); err != nil {
			return
		}
		actorSystem.Queue.EnqueueBroadcasts(segment.StableBroadcasts, actorSystem.Timestamp)
		for _, context := range actorSystem.PersistentContexts {
			context.RegisterStableBroadcasts(segment.StableBroadcasts)
		}

		actorSystem.RunEventLoopWhile(func() bool {
			if actorSystem.Queue.HasEnqueuedEvents() {
				return true
			}
			for _, context := range actorSystem.PersistentContexts {
				if context.Alive && !context.DeliveredAllStableBroadcasts() {
					return true
				}
			}
			return false
		})

		log.Infow("Finished stable sub-segment", "segmentIndex", segmentIndex, "timestamp", actorSystem.Timestamp)

		actorSystem.Phase = Unstable
		actorSystem.Queue.EnqueueBroadcasts(segment.UnstableBroadcasts, actorSystem.Timestamp)
		actorSystem.Queue.EnqueueNodeDeaths(segment.DeathEvents, actorSystem.Timestamp)
		actorSystem.Queue.EnqueueNodeRevivals(segment.ReviveEvents, actorSystem.Timestamp)

		actorSystem.RunEventLoopWhile(func() bool {
			if actorSystem.Queue.HasEnqueuedEvents() {
				return true
			}
			return false
		})

		log.Infow("Finished unstable stable sub-segment", "segmentIndex", segmentIndex, "timestamp", actorSystem.Timestamp)
	}

	actorSystem.Phase = Teardown
	actorSystem.CurrentSegmentIndex++
	actorSystem.CurrentSegment = nil
	teardownTimestamp := actorSystem.Timestamp + interfaces.AvgDeliveryTime*TeardownRounds
	actorSystem.ReviveAll()
	actorSystem.RunEventLoopWhile(func() bool {
		return actorSystem.Timestamp < teardownTimestamp
	})

	err = actorSystem.ValidateAllDeliveries()
	if err == nil {
		err = actorSystem.ValidateStartLeading()
	}
	return
}

var logLevel = zap.LevelFlag("test.zaploglevel", zap.DebugLevel, "set log level")

func RunTests(t *testing.T, broadcasterCtor interfaces.AtomicBroadcasterCtor, tests []TestDescriptor) {
	startTime := time.Now()
	log := zaptest.NewLogger(t, zaptest.WrapOptions(zap.IncreaseLevel(logLevel))).Sugar()

	for _, test := range tests {
		if time.Since(startTime) > TestSuitTimeout {
			t.Errorf("test suit timed out")
		}

		log.Infow("Running", "test", test.TestName)

		t.Run(fmt.Sprintf(test.TestName), func(t *testing.T) {
			resultChannel := make(chan error)
			go func() {
				resultChannel <- RunTest(test, broadcasterCtor, log)
			}()

			select {
			case err := <-resultChannel:
				if err != nil {
					t.Errorf("Test failed: %v", err)
				}

			case <-time.After(TestTimeout):
				t.Errorf("test timed out")
			}
		})
	}
}
