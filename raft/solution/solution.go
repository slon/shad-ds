package solution

import (
	"math"

	"gitlab.com/slon/shad-ds/raft/interfaces"
	"google.golang.org/protobuf/proto"

	"bytes"
	"encoding/binary"
	"fmt"
)

const RpcTimeout = interfaces.AvgDeliveryTime * 3
const PingPeriod = interfaces.AvgDeliveryTime * 10
const LeaderLeaseTimeout = 3 * PingPeriod
const VotingTimeout = interfaces.AvgDeliveryTime * 5
const MaxVotingSplay = interfaces.AvgDeliveryTime * 20

const MaxRetryIndex = 15

type RaftState int

const (
	Candidate RaftState = 0
	Leader    RaftState = 1
	Follower  RaftState = 2
)

// State of the follower on leader.
type FollowerInfo struct {
	outstandingRequest *AppendEntriesReq
	persistedIndex     int64
	retryIndex         int
}

type LeaderState struct {
	followers   map[string]*FollowerInfo
	indexInTerm int64
}

type Broadcaster struct {
	context interfaces.ActorContext
	term    int64

	committedIndex int64
	deliveredIndex int64

	callbacks interfaces.BroadcasterCallbacks

	votedFor string
	state    RaftState

	// Totally ordered log on the node.
	logEntries             []*PersistentLogEntry
	persistentEntryOffsets []int64

	nodeId    int
	nodeCount int

	leaderState   *LeaderState
	electionState *ElectionState

	// Counter used to generate unique request id.
	requestIndex int
}

//////////////////////////////////////////////////////////////////////////////////

func (b *Broadcaster) AtomicBroadcast(broadcastValue int64) {
	switch b.state {
	case Follower:
		b.context.Send(&BroadcastReq{
			RequestId:      b.GenerateRequestId(),
			BroadcastValue: broadcastValue,
		},
			b.votedFor,
		)

	case Leader:
		b.DoBroadcast(&broadcastValue)
	}
}

func (b *Broadcaster) Receive(message interfaces.Message) {
	switch x := message.Payload.(type) {
	case *BroadcastReq:
		b.AtomicBroadcast(x.BroadcastValue)

	case *AppendEntriesReq:
		b.OnReqAppendEntries(x, message.Sender)

	case *AppendEntriesRsp:
		b.OnRspAppendEntries(x, message.Sender)

	case *GetVoteReq:
		b.OnReqGetVote(x, message.Sender)

	case *GetVoteRsp:
		b.OnRspGetVote(x, message.Sender)

	default:
		panic("Unknown message type")
	}
}

//////////////////////////////////////////////////////////////////////////////////

const MaxRandomReqId = 1000000000

func (b *Broadcaster) GenerateRequestId() string {
	b.requestIndex++
	return fmt.Sprintf("%v-%v-%v-%v",
		b.nodeId,
		b.term,
		b.requestIndex,
		b.context.RandomInt(MaxRandomReqId))
}

func (b *Broadcaster) ValidateFollowerTerm(term int64, source string) bool {
	if b.term == term && b.state == Follower {
		return true
	}

	if b.term < term {
		b.StartFollowing(source, term)
	}

	return false
}

func (b *Broadcaster) ValidateLeaderTerm(term int64) bool {
	if b.term == term && b.state == Leader {
		return true
	}

	if b.term < term {
		b.StartElections(term)
	}

	return false
}

func (b *Broadcaster) ValidateLastEntry(lastEntryTerm int64, lastEntryIndex int64) bool {
	if len(b.logEntries) == 0 {
		return true
	}

	entry := b.logEntries[len(b.logEntries)-1]

	if entry.Term < lastEntryTerm {
		return true
	}

	if entry.Term > lastEntryTerm {
		return false
	}

	return int64(len(b.logEntries)) <= lastEntryIndex
}

func (b *Broadcaster) ValidatePrevEntry(prevEntryIndex int64, prevEntryTerm int64) bool {
	if prevEntryIndex < 0 {
		return true
	}

	if int64(len(b.logEntries)) <= prevEntryIndex {
		return false
	}

	entry := b.logEntries[prevEntryIndex]
	return entry.Term == prevEntryTerm
}

func (b *Broadcaster) StartFollowing(leader string, term int64) {
	b.state = Follower
	b.term = term
	b.votedFor = leader

	b.ScheduleLeaderLeaseTimeout()
}

func (b *Broadcaster) StartLeading() {
	leaderState := LeaderState{
		followers:   make(map[string]*FollowerInfo, 0),
		indexInTerm: 0}

	b.state = Leader
	b.leaderState = &leaderState

	for nodeId := 0; nodeId < b.nodeCount; nodeId++ {
		if nodeId != b.nodeId {
			b.leaderState.followers[interfaces.GetActorId(nodeId)] = &FollowerInfo{
				persistedIndex:     int64(len(b.logEntries) - 1),
				retryIndex:         0,
				outstandingRequest: nil}
		}
	}

	b.callbacks.OnStartLeading(b.term)
}

//////////////////////////////////////////////////////////////////////////////////

func (b *Broadcaster) MakeAppendEntriesReq(fromIndex int64) *AppendEntriesReq {
	req := AppendEntriesReq{
		RequestId:      b.GenerateRequestId(),
		Term:           b.term,
		LogEntries:     append([]*PersistentLogEntry{}, b.logEntries[fromIndex:]...),
		CommittedIndex: b.committedIndex,
		PrevIndex:      fromIndex - 1}

	if fromIndex > 0 {
		req.PrevTerm = b.logEntries[fromIndex-1].Term
	} else {
		req.PrevTerm = -1
	}

	return &req
}

func (b *Broadcaster) SendAppendEntriesToFollowers() {
	for actorId, followerInfo := range b.leaderState.followers {
		if followerInfo.outstandingRequest == nil {
			b.SendAppendEntries(actorId, followerInfo)
		}
	}
}

func (b *Broadcaster) SendAppendEntries(actorId string, followerInfo *FollowerInfo) {
	req := b.MakeAppendEntriesReq(followerInfo.persistedIndex + 1)
	followerInfo.outstandingRequest = req
	err := b.context.Send(req, actorId)
	if err != nil {
		b.context.Log().Warnw("Cannot send message, I'm probably dead")
	}

	b.context.AfterFunc(RpcTimeout, func() {
		b.OnAppendEntriesTimeout(actorId, req)
	})
}

func (b *Broadcaster) OnReqAppendEntries(req *AppendEntriesReq, source string) {
	rsp := AppendEntriesRsp{
		RequestId: req.RequestId,
		Success:   false,
	}

	if b.ValidateFollowerTerm(req.Term, source) {
		if b.ValidatePrevEntry(req.PrevIndex, req.PrevTerm) {
			b.logEntries = append(b.logEntries[:req.PrevIndex+1], req.LogEntries...)
			b.SaveEntriesFromIndex(req.PrevIndex + 1)
			rsp.Success = true
			b.DoAdvanceCommittedIndex(req.CommittedIndex)
			b.ScheduleLeaderLeaseTimeout()
		}
	}
	rsp.PrevIndex = int64(len(b.logEntries))
	rsp.Term = b.term
	err := b.context.Send(&rsp, source)
	if err != nil {
		b.context.Log().Warnw("Cannot send message, I'm probably dead")
	}
}

func (b *Broadcaster) OnRspAppendEntries(rsp *AppendEntriesRsp, source string) {
	if !b.ValidateLeaderTerm(rsp.Term) {
		return
	}

	followerInfo := b.leaderState.followers[source]
	if followerInfo.outstandingRequest == nil ||
		followerInfo.outstandingRequest.RequestId != rsp.RequestId {
		return
	}

	req := followerInfo.outstandingRequest
	followerInfo.outstandingRequest = nil
	if rsp.Success {
		followerInfo.retryIndex = 0
		followerInfo.persistedIndex = req.PrevIndex + int64(len(req.LogEntries))
		b.AdvanceLeaderCommittedIndex()

		if int64(len(b.logEntries)) > followerInfo.persistedIndex+1 {
			b.SendAppendEntries(source, followerInfo)
		}
	} else {
		// Adjust persisted index and retry sending.
		if followerInfo.persistedIndex > rsp.PrevIndex {
			followerInfo.persistedIndex = rsp.PrevIndex
		}

		if followerInfo.retryIndex < MaxRetryIndex {
			followerInfo.retryIndex++
		}
		followerInfo.persistedIndex -= int64(math.Pow(2, float64(followerInfo.retryIndex)))

		if followerInfo.persistedIndex < 0 {
			followerInfo.persistedIndex = -1
		}
		b.SendAppendEntries(source, followerInfo)
	}
}

func (b *Broadcaster) OnAppendEntriesTimeout(actorId string, req *AppendEntriesReq) {
	if !b.ValidateLeaderTerm(req.Term) {
		return
	}

	followerInfo := b.leaderState.followers[actorId]
	if followerInfo.outstandingRequest == nil || followerInfo.outstandingRequest.RequestId != req.RequestId {
		return
	}

	followerInfo.outstandingRequest = nil
	b.SendAppendEntries(actorId, followerInfo)
}

//////////////////////////////////////////////////////////////////////////////////

type ElectionState struct {
	req          *GetVoteReq
	successCount int
	votes        map[string]bool
}

func (b *Broadcaster) MakeGetVoteRequest() *GetVoteReq {
	req := GetVoteReq{
		RequestId: b.GenerateRequestId(),
		Term:      b.term}

	req.LastEntryIndex = int64(len(b.logEntries))
	if len(b.logEntries) > 0 {
		lastEntry := b.logEntries[len(b.logEntries)-1]
		req.LastEntryTerm = lastEntry.Term
	} else {
		req.LastEntryTerm = -1
	}

	return &req
}

func (b *Broadcaster) StartElections(term int64) {
	b.term = term
	b.state = Candidate
	b.electionState = nil
	b.leaderState = nil

	splay := interfaces.MinTimerDelay + b.nodeId*5*interfaces.AvgDeliveryTime + b.context.RandomInt(MaxVotingSplay)
	b.context.AfterFunc(splay, func() {
		if b.state != Candidate || b.electionState != nil {
			return
		}

		b.term++
		req := b.MakeGetVoteRequest()
		b.electionState = &ElectionState{
			req:          req,
			successCount: 1,
			votes:        make(map[string]bool)}
		b.votedFor = interfaces.GetActorId(b.nodeId)

		b.SaveMeta()
		for nodeId := 0; nodeId < b.nodeCount; nodeId++ {
			if nodeId != b.nodeId {
				actorId := interfaces.GetActorId(nodeId)
				b.electionState.votes[actorId] = false

				b.context.Send(req, actorId)
			}
		}

		b.context.AfterFunc(interfaces.MinTimerDelay+VotingTimeout, func() {
			b.OnVotingTimeout(req)
		})
	})
}

func (b *Broadcaster) OnReqGetVote(req *GetVoteReq, source string) {
	rsp := GetVoteRsp{RequestId: req.RequestId, VoteGranted: false}

	switch {
	case b.term == req.Term && b.votedFor == source:
		rsp.VoteGranted = true

	case b.term < req.Term:
		if b.ValidateLastEntry(req.LastEntryTerm, req.LastEntryIndex) {
			b.StartFollowing(source, req.Term)
			rsp.VoteGranted = true
		} else {
			b.StartElections(req.Term)
		}
	}

	rsp.Term = b.term
	b.context.Send(&rsp, source)
}

func (b *Broadcaster) OnRspGetVote(rsp *GetVoteRsp, source string) {
	if b.state != Candidate || b.electionState == nil || b.electionState.req.RequestId != rsp.RequestId {
		return
	}

	if b.term < rsp.Term || !rsp.VoteGranted {
		b.StartElections(rsp.Term)
		return
	}

	if !b.electionState.votes[source] {
		b.electionState.votes[source] = true
		b.electionState.successCount++

		if b.electionState.successCount > b.nodeCount/2 {
			b.StartLeading()
		}
	}
}

func (b *Broadcaster) OnVotingTimeout(req *GetVoteReq) {
	if b.state != Candidate || b.electionState == nil || b.electionState.req.RequestId != req.RequestId {
		return
	}

	b.StartElections(req.Term)
}

//////////////////////////////////////////////////////////////////////////////////

const PersistentStateSize = 16

type PersistentState struct {
	CurrentTerm     int64
	TotalEntryCount int64
}

func (b *Broadcaster) SaveMeta() {
	var buffer bytes.Buffer

	ps := PersistentState{
		CurrentTerm:     b.term,
		TotalEntryCount: int64(len(b.logEntries))}

	binary.Write(&buffer, binary.LittleEndian, ps)
	// Errors are possible only if we die.
	b.context.WriteAt(buffer.Bytes(), 0)
}

func (b *Broadcaster) SetPersistentEntryOffset(entryIndex int, offset int64) {
	if entryIndex < len(b.persistentEntryOffsets) {
		b.persistentEntryOffsets[entryIndex] = offset
	} else {
		b.persistentEntryOffsets = append(b.persistentEntryOffsets, offset)
	}

	if entryIndex >= len(b.persistentEntryOffsets) {
		panic(fmt.Errorf("offset indexes are not contiguous"))
	}
}

func (b *Broadcaster) SaveEntriesFromIndex(index int64) {
	offset := int64(PersistentStateSize)
	if index > 0 {
		offset = b.persistentEntryOffsets[index-1]
	}

	for i, entry := range b.logEntries[index:] {
		serializedEntry, err := proto.Marshal(entry)
		if err != nil {
			panic(err)
		}

		size := int64(len(serializedEntry))

		var buffer bytes.Buffer
		binary.Write(&buffer, binary.LittleEndian, size)
		binary.Write(&buffer, binary.LittleEndian, serializedEntry)

		b.context.WriteAt(buffer.Bytes(), offset)
		offset += int64(len(buffer.Bytes()))

		b.SetPersistentEntryOffset(int(index)+i, offset)
	}

	b.SaveMeta()
}

func (b *Broadcaster) LoadEntries(data []byte) {
	reader := bytes.NewReader(data)
	offset := int64(PersistentStateSize)

	for entryIndex := 0; reader.Len() > 0; entryIndex++ {
		var size int64
		binary.Read(reader, binary.LittleEndian, &size)

		serializedMessage := make([]byte, size)
		n, err := reader.Read(serializedMessage)
		if int64(n) != size || err != nil {
			panic("Failed to load serialized message")
		}

		entry := &PersistentLogEntry{}
		if err := proto.Unmarshal(serializedMessage, entry); err != nil {
			panic(err)
		}

		offset += size + int64(binary.Size(size))

		b.logEntries = append(b.logEntries, entry)
		b.SetPersistentEntryOffset(entryIndex, offset)
	}
}

func (b *Broadcaster) ScheduleLeaderLeaseTimeout() {
	logPrefixLength := len(b.logEntries)
	term := b.term

	b.context.AfterFunc(LeaderLeaseTimeout, func() {
		if b.state == Follower && b.term == term && len(b.logEntries) == logPrefixLength {
			b.StartElections(term)
		}
	})
}

func (b *Broadcaster) ScheduleBroadcastDummyMessage() {
	logPrefixLength := len(b.logEntries)
	b.context.AfterFunc(PingPeriod, func() {
		if b.state == Leader && len(b.logEntries) == logPrefixLength {
			b.DoBroadcast(nil)
			b.ScheduleBroadcastDummyMessage()
		}
	})
}

func (b *Broadcaster) DoBroadcast(broadcastValue *int64) {
	b.leaderState.indexInTerm++
	var entry PersistentLogEntry
	if broadcastValue == nil {
		entry = PersistentLogEntry{
			BroadcastValue: -1,
			Heartbeat:      true,
			Term:           b.term}
	} else {
		entry = PersistentLogEntry{
			BroadcastValue: *broadcastValue,
			Heartbeat:      false,
			Term:           b.term}
	}

	b.logEntries = append(b.logEntries, &entry)
	b.SaveEntriesFromIndex(int64(len(b.logEntries)) - 1)
	b.SendAppendEntriesToFollowers()
	b.ScheduleBroadcastDummyMessage()
}

func (b *Broadcaster) AdvanceLeaderCommittedIndex() {
	for {
		if b.committedIndex+1 >= int64(len(b.logEntries)) {
			return
		}

		committedCount := 1
		for _, follower := range b.leaderState.followers {
			if b.committedIndex+1 <= follower.persistedIndex {
				committedCount++
			}
		}

		if committedCount < b.nodeCount/2 {
			return
		}

		b.DoAdvanceCommittedIndex(b.committedIndex + 1)
	}
}

func (b *Broadcaster) DoAdvanceCommittedIndex(committedIndex int64) {
	for i := b.deliveredIndex + 1; i <= committedIndex; i++ {
		if !b.logEntries[i].Heartbeat {
			b.callbacks.Deliver(b.logEntries[i].BroadcastValue)
		}
	}
	if committedIndex > b.committedIndex {
		b.deliveredIndex = committedIndex
		b.committedIndex = committedIndex
	}
}

//////////////////////////////////////////////////////////////////////////////////

func CreateBroadcaster(nodeId, nodeCount int, callbacks interfaces.BroadcasterCallbacks, context interfaces.ActorContext) interfaces.Actor {
	if binary.Size(&PersistentState{}) != PersistentStateSize {
		err := fmt.Errorf("invalid size of persistent state (%v)", binary.Size(PersistentState{}))
		panic(err)
	}

	broadcaster := Broadcaster{
		context:        context,
		callbacks:      callbacks,
		nodeId:         nodeId,
		nodeCount:      nodeCount,
		term:           0,
		committedIndex: -1,
		deliveredIndex: -1,
		requestIndex:   0,
		logEntries:     make([]*PersistentLogEntry, 0)}

	if context.GetStorageLength() > 0 {
		var ps PersistentState
		data := make([]byte, PersistentStateSize)
		_, err := context.ReadAt(data, 0)
		if err != nil {
			return nil
		}
		reader := bytes.NewReader(data)
		binary.Read(reader, binary.LittleEndian, &ps)

		broadcaster.term = ps.CurrentTerm

		data = make([]byte, context.GetStorageLength()-PersistentStateSize)
		n, err := context.ReadAt(data, PersistentStateSize)
		if err != nil {
			broadcaster.committedIndex = -1
		} else {
			if n != len(data) {
				err := fmt.Errorf("failed to read enough from persistent storage (NodeId: %v, Expected: %v, Actual: %v)",
					broadcaster.nodeId,
					len(data),
					n)
				panic(err)
			}

			broadcaster.LoadEntries(data)

			broadcaster.DoAdvanceCommittedIndex(int64(len(broadcaster.logEntries)) - 1)
		}
	}

	broadcaster.StartElections(broadcaster.term)
	return &broadcaster
}
